Introduction
===

This is sample app done in scope of technical task. It allows to search through all repositories on https://github.com using api.github.com. App was built by using SwiftUI and Combine, without any other third party library.


Structure
---
- **GitRepoSearch** contains app logic by itself
- **GitRepoSearchTests** contains few unit tests as example
- **GitRepoSearchUITests** the same as unit tests, few ui test created as example

Requirements
---

- iOS
	- iOS 14+
	- Xcode 12.5
	- Swift 5
- macOS
    - Catalina or Big Sur
